/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.transistorsoft.flutter.backgroundfetch;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.transistorsoft.flutter.backgroundfetch";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = -1;
  public static final String VERSION_NAME = "";
}
