/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.benzneststudios.flutter_rounded_date_picker;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.benzneststudios.flutter_rounded_date_picker";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = -1;
  public static final String VERSION_NAME = "";
}
