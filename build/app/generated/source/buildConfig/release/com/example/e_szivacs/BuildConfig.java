/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.example.e_szivacs;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "io.github.boapps.meSzivacs";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 2050;
  public static final String VERSION_NAME = "2.2.18";
}
